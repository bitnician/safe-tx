import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";

function loadAccounts() {
  return process.env.PRIVATE_KEY !== undefined ? [process.env.PRIVATE_KEY] : [];
}

const config: HardhatUserConfig = {
  solidity: {
    compilers: [
      {
        version: "0.8.9",
        settings: {
          optimizer: {
            enabled: true,
            runs: 900,
          },
        },
      },
    ],
  },
  networks: {
    mainnet: {
      url: `https://mainnet.infura.io/v3/b478a669bf7048d09287e797ee0049e0`,
      accounts: loadAccounts(),
    },
    ropsten: {
      url: `https://ropsten.infura.io/v3/b478a669bf7048d09287e797ee0049e0`,
      accounts: [
        "0xfa390255af130866dbb9b7e3a415479c9b1edfcb41cf8c8e92c191223df05138",
      ],
    },
  },
  gasReporter: {
    enabled: process.env.REPORT_GAS !== undefined,
    currency: "USD",
  },
  etherscan: {
    apiKey: {
      mainnet: process.env.ETHERSCAN_API_KEY as string,
      goerli: process.env.ETHERSCAN_API_KEY as string,
      polygon: process.env.POLYGONSCAN_API_KEY as string,
      avalanche: process.env.SNOWTRACE_API_KEY as string,
      avalancheFujiTestnet: process.env.SNOWTRACE_API_KEY as string,
      bsc: process.env.BSCSCAN_API_KEY as string,
    },
  },
};

export default config;
