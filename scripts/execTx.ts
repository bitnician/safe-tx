import hre, { ethers } from "hardhat";
import safeAbi from "./safeAbi.json";

async function main() {
  const [signer] = await ethers.getSigners();

  const safeAddress = "0x2603F93EE87899ea18f48c112A27C60e4c220C56";
  const usdtAddress = "0x110a13FC3efE6A245B50102D2d79B3E76125Ae83";

  const safeContract = await hre.ethers.getContractAt(safeAbi, safeAddress);

  // Prepare `approve` data
  const usdtAbi = ["function approve(address spender, uint256 amount)"];
  const usdtInterface = new ethers.utils.Interface(usdtAbi);
  const approveData = usdtInterface.encodeFunctionData("approve", [
    "0xD2B8C7991D3aE6BB4f00A0DBB56dEef542C717CA",
    "1000",
  ]);

  const to = usdtAddress;
  const value = 0;
  const data = approveData;
  const operation = 0;
  const safeTxGas = 1000000000;
  const baseGas = 1000000;
  const gasPrice = 2000000000;
  const gasToken = ethers.constants.AddressZero;
  const refundReceiver = signer.address;
  const nonce = 0;

  const types = {
    Permit: [
      { name: "to", type: "address" },
      { name: "value", type: "uint256" },
      { name: "data", type: "bytes" },
      { name: "operation", type: "uint8" },
      { name: "safeTxGas", type: "uint256" },
      { name: "baseGas", type: "uint256" },
      { name: "gasPrice", type: "uint256" },
      { name: "gasToken", type: "address" },
      { name: "refundReceiver", type: "address" },
      { name: "nonce", type: "uint256" },
    ],
  };

  const typeValue = {
    to,
    value,
    data,
    operation,
    safeTxGas,
    baseGas,
    gasPrice,
    gasToken,
    refundReceiver,
    nonce,
  };

  const chainId = "3";
  const domain = {
    chainId,
    verifyingContract: safeAddress,
  };

  const signature = await signer._signTypedData(domain, types, typeValue);

  // execTransaction(address to, uint256 value, bytes data, uint8 operation, uint256 safeTxGas, uint256 gasPrice, address gasToken, address refundReceiver, bytes signatures)

  // get the owners
  const allOwner = await safeContract.getOwners();
  console.log(allOwner);

  const result = await safeContract.execTransaction(
    to,
    value,
    data,
    operation,
    safeTxGas,
    baseGas,
    gasPrice,
    gasToken,
    refundReceiver,
    signature
  );

  console.log(result);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
